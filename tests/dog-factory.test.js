import dogFactory from '../main';
// eslint-disable-next-line new-cap
const df = new dogFactory();

// eslint-disable-next-line no-undef
test('this generate a User Model', () => {
  // eslint-disable-next-line no-undef
  expect(df.create('User', { name: 'firstName', phone: 'phone' })).toBeUndefined();
});

// eslint-disable-next-line no-undef
test('this generate a Color Model', () => {
  // eslint-disable-next-line no-undef
  expect(df.create('Website', { website: 'website' })).toBeUndefined();
});

// eslint-disable-next-line no-undef
test('Associating website model with users & and changing prop in user model', () => {
  // eslint-disable-next-line no-undef
  expect(df.changeProperty('User', { website: df.associateFull('Website', 5) })).toBeUndefined();
});

// eslint-disable-next-line no-undef
test('Building User model', () => {
  // eslint-disable-next-line no-undef
  expect(typeof df.build('User', 2)).toBe('object');
})
