import DataGenerator from './dataGenerator'

export default class {
    
    constructor(){
        this.factories = {};
    }
    /**
     * This function create and store a model
     * @param name {string}
     * @param attr {object}
     * @returns {string}
     */
    create(name, attr) {
        
        if(typeof this.factories[name] !== "undefined"){
            console.warn('This model/scheme already exists');
            return 'This model/scheme already exists';
        } else if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        }
        
        this.factories[name] = attr;
        this.factories[name]['id'] = 'id';
    }
    
    /**
     *  This function allows you to modify the models schema
     * @param name {string}
     * @param newAttr {object}
     * @returns {string|*}
     */
    changeProperty(name, newAttr) {
        if(typeof newAttr !== "object"){
            console.warn('This function only accepts one object as an attribute');
            return 'This function only accepts one object as an attribute'
        } else if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        } else if(this.factories[name] && newAttr) {
            for(let key in newAttr) {
                this.factories[name][key] = newAttr[key];
            }
        }
    }
    
    /**
     * Generally this function should be allowed to obtain only the id
     * but can also give you the information of any property of the scheme
     * @param name {string}
     * @param prop {string}
     * @returns {string|*}
     */
    association(name, prop){
        if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        } else if (typeof prop !== "string"){
            console.warn('The second parameter must be a string');
            return 'The second parameter must be a string';
        }
        
        let tmp = this.build(name)
        for(let key in tmp){
            if(key === prop){
                return tmp[key]
            }
        }
    }
    
    /**
     * This function return an array of ids
     * @param name {string}
     * @param number {number}
     * @returns {string|Array}
     */
    associateMany(name, number){
        if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        } else if (typeof number !== "number"){
            console.warn('The second parameter must be a number');
            return 'The second parameter must be a number';
        }
        
        let tmpArray = [];
        for(let i = 0; i < number; i++){
            let tmp = this.build(name);
            for(let key in tmp){
                if(key === 'id'){
                    tmpArray.push(tmp[key]);
                }
            }
        }
        return tmpArray;
    }
    
    
    /**
     * Returns an object or an array of objects
     * @param name {string}
     * @param number {number}
     * @returns {string|Array}
     */
    associateFull(name, number = 1){
        if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        } else if (typeof number !== "number"){
            console.warn('The second parameter must be a number');
            return 'The second parameter must be a number';
        }
        
        let tmp = [];
        for(let i = 0; i < number; i++){
             tmp.push(this.build(name));
        }
        return number === 1 ? tmp[0] : tmp;
        
    }
    
    
    /**
     * returns an object based on the model
     * @param name {string}
     * @param quantity {number}
     * @returns {string|{}|Array}
     */
    build(name, quantity = 1) {
    
        if (typeof name !== "string"){
            console.warn('The first parameter must be a string');
            return 'The first parameter must be a string';
        } else if (typeof quantity !== "number"){
            console.warn('The second parameter must be a number');
            return 'The second parameter must be a number';
        }
        
        if(quantity === 1){
            return DataGenerator(this.factories[name])
        } else {
            let tmp = [];
            for(let i = 0; i < quantity;i++){
                tmp.push(DataGenerator(this.factories[name]))
            }
            return tmp;
        }
        
    }
}