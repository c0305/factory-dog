import faker from 'faker';

export default {
  id: faker.random.uuid,
  price: faker.commerce.price,
  phrase: faker.hacker.phrase,
  paragraph: faker.lorem.paragraph,
  name: faker.name.findName,
  firstName: faker.name.firstName,
  lastName: faker.name.lastName,
  phone: faker.phone.phoneNumber,
  email: faker.internet.email,
  address: faker.address.streetAddress,
  state: faker.address.state(),
  city: faker.address.city(),
  zipcode: faker.address.zipCode(),
  age: ageNumber,
  website: faker.internet.url,
  companyName: faker.company.companyName(),
  recentDate: faker.date.recent,
  futureDate: faker.date.future,
  pastDate: faker.date.past,
}

function ageNumber(){
  return Math.round(Math.random() * (60 - 18) + 18)
}