import Glosary from "./glosary";

export default function(attr) {
  let tmp = {};
  for(var keyAttr in attr){
    for(let keyGlosary in Glosary){
      if(attr[keyAttr] === keyGlosary){
        tmp[keyAttr] = Glosary[keyAttr]();
        break;
      } else if(typeof attr[keyAttr] === 'function'){
        tmp[keyAttr] = attr[keyAttr]()
        break;
      } else {
        tmp[keyAttr] = attr[keyAttr]
      }
  
    }
  }
  return tmp;
}